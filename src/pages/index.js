import * as React from "react";
import { useState, useEffect } from "react";

let links = require("../resources/knowlegeArticles.json");

const pageStyles = {
  color: "#232129",
  padding: 96,
  fontFamily: "-apple-system, Roboto, sans-serif, serif",
};
const headingStyles = {
  marginTop: 0,
  marginBottom: 64,
  maxWidth: 320,
};

const cardContainer = {
  display: "flex",
  flexWrap: "wrap",
  justifyContent: "space-evenly",
};

const listItemStyles = {
  fontWeight: 300,
  fontSize: 24,
  maxWidth: 560,
  marginBottom: 30,
  flex: "0 1 20%",
  border: "1px solid black",
};

const linkStyle = {
  color: "#8954A8",
  fontWeight: "bold",
  fontSize: 16,
  verticalAlign: "5%",
};

const descriptionStyle = {
  color: "#232129",
  fontSize: 14,
  marginTop: 10,
  marginBottom: 0,
  lineHeight: 1.25,
};

const badgeStyle = {
  color: "#fff",
  backgroundColor: "#088413",
  border: "1px solid #088413",
  fontSize: 11,
  fontWeight: "bold",
  letterSpacing: 1,
  borderRadius: 4,
  padding: "4px 6px",
  display: "inline-block",
  position: "relative",
  top: -2,
  marginLeft: 10,
  lineHeight: 1,
};

const IndexPage = () => {
  const [filterString, setFilterString] = useState("");

  const handleSelectedChange = (event) => {
    setFilterString(event.target.value);
  };

  // todo = update to useState for array of KA
  useEffect(() => {
    const filterKA = () => {
      if (filterString === "All") {
        links = links;
      } else {
        links = links.filter((links) => links.functionalArea !== filterString);

        console.log(links);
      }
    };

    filterKA();
  }, [filterString, links]);

  return (
    <main style={pageStyles}>
      <h1 style={headingStyles}>ADCP Self Services</h1>

      <label>
        Filter
        <select value={filterString} onChange={handleSelectedChange}>
          <option valuse="All">All</option>
          <option value="Documentation">Documentation</option>
          <option value="Templates">Templates</option>
        </select>
      </label>

      <div style={cardContainer}>
        {links.map((link) => (
          <div key={link.text} style={{ ...listItemStyles }}>
            <p>{link.functionalArea}</p>
            <span>
              <a
                style={linkStyle}
                href={`${link.url}?utm_source=starter&utm_medium=start-page&utm_campaign=minimal-starter`}
              >
                {link.text}
              </a>
              {link.badge && (
                <span style={badgeStyle} aria-label="New Badge">
                  NEW!
                </span>
              )}
              <p style={descriptionStyle}>{link.description}</p>
            </span>
          </div>
        ))}
      </div>
      {/* <img
        alt="Gatsby G Logo"
        src="data:image/svg+xml,%3Csvg width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M12 2a10 10 0 110 20 10 10 0 010-20zm0 2c-3.73 0-6.86 2.55-7.75 6L14 19.75c3.45-.89 6-4.02 6-7.75h-5.25v1.5h3.45a6.37 6.37 0 01-3.89 4.44L6.06 9.69C7 7.31 9.3 5.63 12 5.63c2.13 0 4 1.04 5.18 2.65l1.23-1.06A7.959 7.959 0 0012 4zm-8 8a8 8 0 008 8c.04 0 .09 0-8-8z' fill='%23639'/%3E%3C/svg%3E"
      /> */}

      <h2>Still need help? </h2>

      <button>
        <a
          href={
            "https://pages-test-bmantenuto-test1-5db3ddb34c77a61897246126945cd35f0c8.gitlab.io/"
          }
        >
          Submit Help Ticket Here
        </a>
      </button>
    </main>
  );
};

export default IndexPage;

export const Head = () => <title>Home Page</title>;
